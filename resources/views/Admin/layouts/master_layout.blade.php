<!DOCTYPE html>
@include('Admin.header.header')
<style type="text/css">
  .chg-nav > li > a:hover, .chg-nav > li > a:focus, .chg-nav > li > a:active, .chg-nav > li > a:visited, .chg-nav > li > a:visited, .chg-nav > li > a:link {
  text-decoration: none;
  background-color: initial!important;
  }
  body {
    font-size: 0.85rem;
  }
</style>
<body class="fix-header fix-sidebar card-no-border">
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="loader">
      <div class="loader__figure"></div>
      <p class="loader__label">DEN Network</p>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
      <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
          <a class="navbar-brand" href="index.html">
            <!-- Logo icon -->
            <b>
              <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
              <!-- Dark Logo icon -->
              <img src="/assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
              <!-- Light Logo icon -->
              <img src="/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
            </b>
            <!--End Logo icon -->
            <!-- Logo text -->
            <span>
              <!-- dark Logo text -->
              <img src="/assets/images/logo-text.png" alt="homepage" class="dark-logo" />
              <!-- Light Logo text -->    
              <img src="/assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
            </span>
          </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
          <!-- ============================================================== -->
          <!-- toggle and nav items -->
          <!-- ============================================================== -->
          <ul class="navbar-nav mr-auto">
            <!-- This is  -->
            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
          </ul>
          <!-- ============================================================== -->
          <!-- User profile and search -->
          <!-- ============================================================== -->
          <ul class="navbar-nav my-lg-0">
            <!-- ============================================================== -->
            <!-- Search -->
            <!-- ============================================================== -->
            <li class="nav-item hidden-xs-down search-box">
              <a class="nav-link hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
              <form class="app-search">
                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> 
              </form>
            </li>
            <!-- ============================================================== -->
            <!-- Profile -->
            <!-- ============================================================== -->
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
              <div class="dropdown-menu dropdown-menu-right animated flipInY">
                <ul class="dropdown-user">
                  <li>
                    <div class="dw-user-box">
                      <div class="u-img"><img src="/assets/images/users/1.jpg" alt="user"></div>
                      <div class="u-text">
                        <h4>{{Auth::user()->name}}</h4>
                        <p class="text-muted">{{Auth::user()->email}}</p>
                       {{-- <a href="pages-profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a>--}}
                      </div>
                    </div>
                  </li>
                  <li role="separator" class="divider"></li>
                  {{--<li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                                      <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                                      <li><a href="#"><i class="ti-email"></i> Inbox</a></li>--}}
                                                      <li role="separator" class="divider"></li>
                                                      <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                                      <li role="separator" class="divider"></li>
                  <li><a href="/superadmin/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
      <!-- Sidebar scroll-->
      <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
          <ul id="sidebarnav">
            <li class="user-profile">
              <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><img src="/assets/images/users/profile.png" alt="user" /><span class="hide-menu">{{Auth::user()->name}} </span></a>
              <ul aria-expanded="false" class="collapse">
                {{--<li><a href="javascript:void()">My Profile </a></li>
                                                <li><a href="javascript:void()">My Balance</a></li>
                                                <li><a href="javascript:void()">Inbox</a></li>
                                                <li><a href="javascript:void()">Account Setting</a></li>--}}
                <li><a href="/superadmin/logout">Logout</a></li>
              </ul>
            </li>
            <li class="nav-devider"></li>
            <li class=""> <a class="" href="/superadmin/dashboard"><i class="mdi mdi-gauge"></i> Dashboard </a>
            <li class=""> <a class="" href="/superadmin/groups"><i class="mdi mdi-account-multiple"></i> Groups </a>
            <li class=""> <a class="" href="/superadmin/lcos"><i class="mdi mdi-account"></i> LCO </a>
            <li class=""> <a class="" href="/superadmin/boxes"><i class="mdi mdi-dropbox"></i> Boxes </a>
            </li>
            <li class=""> <a class="" href="/superadmin/billing"><i class="mdi mdi-file-document"></i> Billing </a>
            </li>
          </ul>
        </nav>
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <div class="page-wrapper">
      @yield('content')
      <footer class="footer"> © {{date('Y')}} DEN Network </footer>
      <!-- ============================================================== -->
      <!-- End footer -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
  </div>
  @include('Admin.footer.footer')
</body>
</html>