@extends('Admin.layouts.master_layout')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Groups</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Groups</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Total ({{$groups->total()}}) <span class="pull-right"><button class="btn btn-info btn-sm" name="showForm">Add {{--<i class="fa fa-plus"></i>--}}</button></span></h4>
                    <div id="add-group" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Action</h4> 
                                    <a class="close pull-right" data-dismiss="modal" aria-hidden="true">×</a></div>
                                <div class="modal-body">
                                    <div name='group-insert'>
                                        <div class="form-group">
                                        <label class="control-label">Group Name</label>
                                        <input type="text" id="name" required='true' class="form-control" > </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info waves-effect" name="btninsert" >Save</button>
                                    <button type="button" class="btn btn-info waves-effect" name="btnupdate" style="display: none" >Update</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" name="groups">
                            <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Name</th>
                                    <th>Created On</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp 
                            @foreach ($groups as $group)
                                <tr id="{{$group->id}}">
                                    <td>{{ (( $groups->currentPage() - 1) * 10 ) + $i++ }}</td>
                                    <td>{{$group->name}}</td>
                                    <td>{{$group->created_at}}</td>
                                    <td class="text-nowrap">
                                        <a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>&nbsp;<a href="javascript:void(0);" name="btnRowDelete" class="text-danger" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if (!$groups->count())
                            <tr><td  > No groups found</td></tr>
                            @endif
                            </tbody>
                        </table>
                        <ul class="pagination pagination-sm no-margin pull-right"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src='/admin/jscontrols/groups.js'></script>
<script>var currentpage = 1;</script>
<script> 
  $('.pagination').twbsPagination({
      totalPages: Math.ceil("{{ $groups->total() / 10 }}"),
      startPage:parseInt("{{ $groups->currentPage() }}"),
      initiateStartPageClick:false,
      visiblePages: 10,
      onPageClick: function (event, page) {
        var getdata = {};
        getdata['page'] = page;
        var display = "";
        // $.loader.start(); 
        $.get('/superadmin/groups/get-data',getdata,function(response){
          var display = "";
          for(var i=0; i<response.data.length; i++){
            // $.loader.stop();
             display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((page - 1) * 10)) +
                        '</td><td > '+response.data[i].name+'</td><td >' +
                        response.data[i].created_at + '</td><td ><a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-inverse m-r-10"></i></a>'+
                        '&nbsp;<a href="javascript:void(0);" name="btnRowDelete" class="text-danger" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-close text-danger"></i></a>' +
                        '</td>'+
                        '</tr>';
          }

          $('table[name=groups] tbody').html(display);
        
      },'json');

        history.pushState('', '', '/superadmin/groups?page=' + page);
        currentpage = page;
      }
  });
</script>
@endsection