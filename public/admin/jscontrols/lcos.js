$(function() {

    $.lcos = {
        init: function(callback) {
            var files;

            $(document).on('click', '[name="btnRowView"]', function() {
                $.lcos.view($(this));
            });

            $(document).on('click', '[name="send"]', function() {
                $.lcos.box($(this));
            });


            $(document).on('click', '[name="btnRowEdit"]', function() {
                $.lcos.edit($(this));
            });

            $(document).on('keyup change', '#add-lco input, #add-lco select', function() {
                if ($(this).val()) {
                    $(this).parents('.form-group').removeClass('has-danger');
                }
            });

            $(document).on('click', '[name=btnupdate]', function() {
                $.lcos.update($(this));
            });
            
            $(document).on('click', '[name=btncancel]', function() {
                $.lcos.back($(this));
            });

            $(document).on('change', '[name="lco_file"]', function() {
                var fil_name = $(this).parents("[name='lco-uplod']").find('[name="js_lco_name"]');
                var reader = new FileReader();
                fil_name.text(this.files[0].name);
                files = this.files;
            });

            $(document).on('click', '[name="showImport"]', function() {
                $.lcos.showImport();
            });

            $(document).on('click', '[name="showForm"]', function() {
                $.lcos.showForm();
            });

            $(document).on('click', '[name=btncancel]', function() {
                $.lcos.back($(this));
            });

            $(document).on('click', '[name=btninsert]', function() {
                $.lcos.insert($(this));
            });

            $(document).on('click', '[name=btnImport]', function() {
                $.lcos.import($(this), files);
            });

            $(document).on('click', '[name=btnRowDelete]', function() {
                $.lcos.delete($(this));
            });
        },
        box: function(ths, callback) {
            var proceed = true;
            $('[name=boxes] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).parents('.form-group').addClass('has-danger');
                    proceed = false;
                }
            });
            
            if (proceed) {
                var postdata = new FormData();
                postdata.append('sd_box', $('#sd_box').val());
                postdata.append('hd_sd_box', $('#hd_sd_box').val());
                postdata.append('hd_hd_box', $('#hd_hd_box').val());
                postdata.append('id', $('[name="send"]').attr("id"));
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/lcos/box_add",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();

                        if (response.status) {
                            $('form[name=boxes]').get(0).reset();
                            $('[name="lco-display"]').show();
                            $('#add-lco').hide();
                            swal("Success!", 'Successfully updated', "success");
                            $('#exampleModal').modal('hide');

                            // var display = response.message;
                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'green',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-green',
                            //             action: function() {
                            //                 $('form[name=tip-insert]').get(0).reset();
                            //                 $('[name="tip-display"]').show();
                            //                 $('[name="tip-edit"]').hide();
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $.lcos.display($.lcos.query_params('page'));
                            //                 }, 1000);

                            //             }
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = value[0]; //fetch oth(first) error.

                            //     });
                            // } else {
                            //     display = response.message;
                            // }
                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         close: function() {}
                            //     }
                            // });
                        }
                    }
                });
            }
        },

        view: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();

            $.ajax({
                url: '/superadmin/lcos/view/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {

                    $('#viewModal').find('.modal-title').text('View LCO Details');
                    $('#viewModal').find('#lco_num').text(response.lco_num);
                    $('#viewModal').find('#lco_name').text(response.name);
                    $('#viewModal').find('#group').text(response.group.name);
                    $('#viewModal').find('#gst_no').text(response.gst_num);
                    $('#viewModal').find('#email').text(response.email);
                    $('#viewModal').find('#mobile').text(response.mobile);
                    $('#viewModal').find('#address').text(response.address);
                    $('#viewModal').find('#city').text(response.city);
                    $('#viewModal').find('#district').text(response.district);
                    $('#viewModal').find('#pin_code').text(response.pin_code);

                    $("#viewModal").modal('show');
                },
                error: function() { /* error code goes here*/ }
            });
        },
        display: function(offset) {
            offset = parseInt(offset) ? offset : '1';
            //display companys
            // $.loader.start();
            
            $.get('/superadmin/lcos/get-data?page=' + offset, function(response) {
                // $.loader.stop();
                var display = "";
                
                for (var i = 0; i < response.data.length; i++) {
                    display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((offset - 1) * 10)) +'</td> '+
                        '<td > '+response.data[i].lco_num+'</td>' +
                        '<td > '+response.data[i].group.name+'</td>' +
                        '<td > '+response.data[i].name+'</td>' +
                        '<td > '+response.data[i].mobile+'</td>' +
                        '<td >' +response.data[i].email + '</td><td ><a href="javascript:void(0);" name="btnRowView" data-toggle="tooltip" data-original-title="View details"> <i class="fa fa-eye text-inverse m-r-10"></i></a>'+
                        '<a href="javascript:void(0);" name="btnRowMap" data-toggle="tooltip" data-original-title="Assign Boxes"> <i class="fa fa-sitemap text-inverse m-r-10"></i></a>'+
                        '<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i></a>'+
                        '<a href="javascript:void(0);" name="btnRowDelete" class="text-danger" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i></a>'+
                        '</td>'+
                        '</tr>';
                }

                display = display == "" ? '<tr><th colspan=4 class="text-center">No LCOs found </th></tr>' : display;

                // alert(display);
                $('[name="lcos"] tbody').html(display);
            }, 'json');
        },
        back: function($this) {
            $('.form-group').removeClass('has-danger');
            $('form[name=lco-insert]').get(0).reset();
            $('[name="lco-display"]').show();
            $('#add-lco').hide();
        },
        query_params: function(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },
        update: function(ths) {
            var id = ths.val();

            var proceed = true;

            $('[name=lco-insert] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).parents('.form-group').addClass('has-danger');
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                postdata.append('lco_num', $('#lco_num').val());
                postdata.append('name', $('#lco_name').val());
                postdata.append('gst_num', $('#gst_no').val());
                postdata.append('group_id', $('#group_id').val());
                postdata.append('email', $('#email').val());
                postdata.append('mobile', $('#mobile').val());
                postdata.append('district', $('#district').val());
                postdata.append('address', $('#address').val());
                postdata.append('city', $('#city').val());
                postdata.append('pin_code', $('#pin_code').val());

                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/lcos/update/" + id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.status) {
                            $('form[name=lco-insert]').get(0).reset();
                            $('[name="lco-display"]').show();
                            $('#add-lco').hide();
                            var display = response.message;
                            swal("Success!", 'Successfully updated', "success");

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'blue',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-primary',
                            //             action: function() {
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $('form[name=tip-insert]').get(0).reset();
                            //                     $('[name="tip-display"]').show();
                            //                     $('[name="tip-edit"]').hide();
                            //                     $.lcos.display($.lcos.query_params('page'));
                            //                 }, 1000)
                            //             }
                            //         },
                            //         // close: function () {
                            //         // }
                            //     }
                            // });

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            //     });
                            // } else {
                            //     display = response.message;
                            // }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'Error',
                            //             btnClass: 'btn-red',
                            //             action: function() {}
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        }
                    }
                });
            }
        },
        showForm: function(ths) {
            $('#add-lco').find('[name="form-title"]').text('Add New LCO');
            $('#add-lco').find('[name=btninsert]').show();
            $('#add-lco').find('[name=btnupdate]').attr('value', '').hide();
            $('[name="lco-display"]').hide();
            $('form[name=lco-insert]').get(0).reset();
            $("#add-lco").show();
        },
        showImport: function(ths) {
            $('#upload-lco').find('#myModalLabel').text('Import LCOs');

            $('#upload-lco').find('#name').val('');
            $('#upload-lco').find('[name=btnImport]').show();
            $('#upload-lco').modal('show');
        },
        edit: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();

            $.ajax({
                url: '/superadmin/lcos/view/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    $('form[name=lco-insert]').get(0).reset();
                    $('#add-lco').find('[name="form-title"]').text('Edit LCO Details');
                    $('#add-lco').find('#lco_num').val(response.lco_num);
                    $('#add-lco').find('#lco_name').val(response.name);
                    $('#add-lco').find('#group_id').val(response.group_id);
                    $('#add-lco').find('#gst_no').val(response.gst_num);
                    $('#add-lco').find('#email').val(response.email);
                    $('#add-lco').find('#mobile').val(response.mobile);
                    $('#add-lco').find('#address').val(response.address);
                    $('#add-lco').find('#city').val(response.city);
                    $('#add-lco').find('#district').val(response.district);
                    $('#add-lco').find('#pin_code').val(response.pin_code);
                    $('#add-lco').find('[name=btninsert]').hide();
                    $('#add-lco').find('[name=btnupdate]').attr('value', response.id).show();
                    $('[name="lco-display"]').hide();
                    $("#add-lco").show();
                },
                error: function() { /* error code goes here*/ }
            });
        },
        delete: function(ths) {
            var id = ths.closest('tr').attr('id');
            var postdata = new FormData();
            postdata.append('id', id);
            postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this LCO",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                $.ajax({
                    url: '/superadmin/lcos/delete',
                    type: 'POST',
                    async: !1,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.status) {
                            $('[name="lcos"] tbody tr#' + id).css('background-color', 'rgb(181, 117, 117)').hide(1000);
                            setTimeout(function() {
                                if ($('[name="lcos"] tbody tr:visible').length == 0) {
                                    var display = "<tr><th colspan=4 style='color:red'>No lcos Found </th></tr>";
                                    $('[name="lcos"] tbody').html(display);
                                }
                            }, 1001);
                            // display = response.message;
                            display = "LCO has been deleted.";

                            swal("Success!", display, "success");
                        } else {
                            var display = "Some error occured";
                            if ((typeof response.message) == 'object') {
                                $.each(response.message, function(key, value) {
                                    display =  value[0];
                                });
                            } else {
                                display = response.message;
                            }
                            swal(display);
                        }

                    }
                });
            });
            
        },
        insert: function(ths, callback) {
            var proceed = true;
            $('[name=lco-insert] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).parents('.form-group').addClass('has-danger');
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                postdata.append('lco_num', $('#lco_num').val());
                postdata.append('name', $('#lco_name').val());
                postdata.append('gst_num', $('#gst_no').val());
                postdata.append('group_id', $('#group_id').val());
                postdata.append('email', $('#email').val());
                postdata.append('mobile', $('#mobile').val());
                postdata.append('district', $('#district').val());
                postdata.append('address', $('#address').val());
                postdata.append('city', $('#city').val());
                postdata.append('pin_code', $('#pin_code').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/lcos/add",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();

                        if (response.status) {
                            $('form[name=lco-insert]').get(0).reset();
                            $('[name="lco-display"]').show();
                            $('#add-lco').hide();
                            swal("Success!", 'Successfully updated', "success");

                            // var display = response.message;
                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'green',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-green',
                            //             action: function() {
                            //                 $('form[name=tip-insert]').get(0).reset();
                            //                 $('[name="tip-display"]').show();
                            //                 $('[name="tip-edit"]').hide();
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $.lcos.display($.lcos.query_params('page'));
                            //                 }, 1000);

                            //             }
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = value[0]; //fetch oth(first) error.

                            //     });
                            // } else {
                            //     display = response.message;
                            // }
                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         close: function() {}
                            //     }
                            // });
                        }
                    }
                });
            }
        },
        import: function(ths, files) {
            var proceed = true;
            $('[name=lco-uplod] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    swal('No file found');
                    
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                $.each(files, function(key, value) {
                    postdata.append('lco_file', value);
                });

                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/lcos/upload",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();

                        if (response.status) {
                            $('#upload-lco').modal('hide');
                            var display = response.message;

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'green',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-green',
                            //             action: function() {
                            //                 $('form[name=tip-insert]').get(0).reset();
                            //                 $('[name="tip-display"]').show();
                            //                 $('[name="tip-edit"]').hide();
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $.lcos.display($.lcos.query_params('page'));
                            //                 }, 1000);

                            //             }
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        } else {
                            var display = "";
                            if ((typeof response.message) == 'object') {
                                $.each(response.message, function(key, value) {
                                    display = value[0]; //fetch oth(first) error.

                                });
                            } else {
                                display = response.message;
                            }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         close: function() {}
                            //     }
                            // });
                        }
                        swal(display);

                    }
                });
            }
        }
    }
    $.lcos.init();
}(jQuery));