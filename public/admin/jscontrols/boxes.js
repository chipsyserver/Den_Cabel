$(function() {

    $.boxes = {
        init: function(callback) {
            $(document).on('click', '[name="btnRowEdit"]', function() {
                $.boxes.edit($(this));
            });

            $(document).on('click', '[name=btnupdate]', function() {
                $.boxes.update($(this));
            });
            
            $(document).on('click', '[name=btncancel]', function() {
                $.boxes.back($(this));
            });
        },
        display: function(offset) {
            offset = parseInt(offset) ? offset : '1';
            //display companys
            // $.loader.start();
            
            $.get('/superadmin/boxes/get-data?page=' + offset, function(response) {
                // $.loader.stop();
                var display = "";
                
                for (var i = 0; i < response.data.length; i++) {
                    display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((offset - 1) * 10)) +'</td> '+
                        '<td > '+response.data[i].name+'</td>' +
                        '<td > '+response.data[i].subscription+'</td>' +
                        '<td > '+response.data[i].maintenance+'</td>' +
                        '<td >'+
                        '<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i></a>'+
                        '</td>'+
                        '</tr>';
                }

                display = display == "" ? '<tr><th colspan=4 class="text-center">No boxes found </th></tr>' : display;

                // alert(display);
                $('[name="boxes"] tbody').html(display);
            }, 'json');
        },
        query_params: function(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },
        update: function(ths) {
            var id = ths.val();

            var proceed = true;

            $('[name=box-insert] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                postdata.append('subscription', $('#sub_chrgs').val());
                postdata.append('maintenance', $('#maint_chrgs').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/boxes/update/" + id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.status) {
                            $('#add-box').modal('hide');
                            $.boxes.display($.boxes.query_params('page'));
                            
                            var display = response.message;

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'blue',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-primary',
                            //             action: function() {
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $('form[name=tip-insert]').get(0).reset();
                            //                     $('[name="tip-display"]').show();
                            //                     $('[name="tip-edit"]').hide();
                            //                     $.boxes.display($.boxes.query_params('page'));
                            //                 }, 1000)
                            //             }
                            //         },
                            //         // close: function () {
                            //         // }
                            //     }
                            // });

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            //     });
                            // } else {
                            //     display = response.message;
                            // }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'Error',
                            //             btnClass: 'btn-red',
                            //             action: function() {}
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        }
                    }
                });
            }
        },
        edit: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();

            $.ajax({
                url: '/superadmin/boxes/view/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    // $.loader.stop();
                    $('#add-box').find('#myModalLabel').text('Edit box');

                    $('#add-box').find('#name').val(response.name);
                    $('#add-box').find('#sub_chrgs').val(response.subscription);
                    $('#add-box').find('#maint_chrgs').val(response.maintenance);
                    $('#add-box').find('[name=btnupdate]').attr('value', response.id).show();
                    $('#add-box').modal('show');
                },
                error: function() { /* error code goes here*/ }
            });
        },
        back: function($this) {
            $('form[name=tip-insert]').get(0).reset();
            $('[name="tip-display"]').show();
            $('[name="tip-edit"]').hide();
        }
    }
    $.boxes.init();
}(jQuery));