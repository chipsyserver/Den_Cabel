<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['namespace' => 'Web\Admin'], function () {
    Route::get('/superadmin/logout', 'AdminControl@signout_func');
    Route::group(['middleware' => ['guest']], function () {
        Route::get('/', 'AdminControl@index');
        Route::get('/superadmin/', 'AdminControl@index');
        Route::post('/superadmin/auth-login', 'AdminControl@authlogin_func');
    });

    Route::group(['prefix' => 'superadmin'], function () {
        Route::group(['middleware' =>'auth'], function () {
            Route::get('/settings', 'DashboardControl@settings');
            Route::post('/sub-settings', 'DashboardControl@change_settings');
            Route::get('/dashboard', 'DashboardControl@index');
            Route::group(['prefix' => 'groups'], function () {
                Route::get('/', 'GroupControl@index');
                Route::get('/get-data', 'GroupControl@getData');
                Route::get('/view/{id}', 'GroupControl@get');
                Route::post('/update/{id}', 'GroupControl@update');
                Route::post('/delete', 'GroupControl@delete');
                Route::post('/add', 'GroupControl@add');
            });


            Route::group(['prefix' => 'lcos'], function () {
                Route::get('/', 'OperatorControl@index');
                Route::get('/get-data', 'OperatorControl@getData');
                Route::get('/view/{id}', 'OperatorControl@get');
                Route::post('/update/{id}', 'OperatorControl@update');
                Route::post('/delete', 'OperatorControl@delete');
                Route::post('/add', 'OperatorControl@add');
                 Route::post('/box_add', 'OperatorControl@box_add');
                  Route::get('/lco-boxes/{id}', 'OperatorControl@get_lco_boxes');
                Route::get('/download/{filename}', 'OperatorControl@getDownload');
                Route::post('/upload', 'OperatorControl@up_lcos');
            });

            Route::group(['prefix' => 'boxes'], function () {
                Route::get('/', 'BoxControl@index');
                Route::get('/get-data', 'BoxControl@getData');
                Route::get('/view/{id}', 'BoxControl@get');
                Route::post('/update/{id}', 'BoxControl@update');
            });

            Route::group(['prefix' => 'billing'], function () {
                Route::get('/', 'OperatorControl@index');
                Route::get('/get-data', 'OperatorControl@getData');
                Route::get('/view/{id}', 'OperatorControl@get');
                Route::post('/update/{id}', 'OperatorControl@update');
                Route::post('/delete', 'OperatorControl@delete');
                Route::post('/add', 'OperatorControl@add');
            });
        });
    });
});
