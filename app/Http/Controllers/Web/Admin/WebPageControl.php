<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\Repository\Lib\TimeControl;

class WebPageControl extends Controller
{
     public function index(){
            return view('Website.welcome');
     }



     public function posts(){
         $get = DB::collection('post_collection')->orderby('_id','desc')->get();
          // die(json_encode(  $get ));
         $info['data'] =array();
        
         foreach($get as &$value) {
            $value['timeago']   = TimeControl::time_ago($value['update_at']);
            $value['downcount'] = $value['upcount']=0;
           // die(json_encode( $value['rating'] ));
           foreach($value['rating'] as &$rate) {
            switch ($rate['rate']) {
                  case 1 :
                   $value['downcount']++;
                    break;
                 case 0 :
                     $value['upcount']++;
                    break;
                
               }
             }


            $value['cmt_count'] = count($value['comments']);
            array_push($info['data'],$value);
           }


           return view('Website.postslisting')->with("response",$info['data']);
    }
  
     public function post(Request $request,$id){
         //$_SESSION["response_code"];

        $res['data']=DB::collection('post_collection')->where('_id', $id)->get();
        $res['upcount']=$res['downcount']=0;
        $res['cmt_count'] = count( $res['data'][0]['comments']);
        foreach ($res['data'][0]['rating'] as $rating) {
            switch ($rating['rate']) {
                  case 1:
                        $res['upcount']++;
                    break;
                 case 0:
                       $res['downcount']++;
                    break;
             
            }
           
        }
          
        return view('Website.post_sucess')->with("response",$res);
    }
  
}
