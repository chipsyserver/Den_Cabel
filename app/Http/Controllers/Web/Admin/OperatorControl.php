<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ico_boxes;
use Validator;
use DB;
use Excel;

class OperatorControl extends Controller
{
    public function index()
    {
        $data = \App\CableOperator::query();
        foreach ($data as $val) {
            $val->group = $val->group;
        }
        $data = $data->paginate(10);
        return view('Admin.pages.lco', ['lcos' => $data]);
    }

    public function get(Request $request, $id)
    {
        $data = \App\CableOperator::find($id);
        $data->group = $data->group;
        return $data;
    }

    public function get_lco_boxes(Request $request,$id)
    {
        $data = \App\Ico_boxes::where('lco_id',$id)->get();
        
        foreach ($data as $key ) {
          $key->box=$key->box_qty;
        }
        return $data;
    }

 public function getData(Request $request)
    {
        $data = \App\CableOperator::query();
        $data = $data->paginate(10);
        foreach ($data as $val) {
            $val->group = $val->group;
        }
        return $data;
    }

     public function box_add(Request $request)
    {
        $rules = array(
            'sd_box' => 'required','hd_sd_box' => 'required','hd_hd_box' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } else {
            $data = $request->all();
            $list = \App\Ico_boxes::where('lco_id',$data['id'])->get();
           $li = \App\Ico_boxes::where('lco_id',$data['id'])->first();
            if($li){
            $i=0;
            foreach ($list as $key ) {
                $i++;
               if($i==1){$qty=$data['sd_box'];}
               elseif ($i==2) {$qty=$data['hd_sd_box'];}
               else{$data['hd_hd_box'];}
                $dt=\App\Ico_boxes::where('id',$key->id)->where('box_id',$i)->first();
                
               $dt->box_qty=$qty;
                $dt->save();
            }
        }
        else{
            for($i=1;$i<=3;$i++){
            $dt=new Ico_boxes;
            $dt->box_id=$i;
            if($i==1){$dt->box_qty=$data['sd_box'];}
            elseif($i==1){$dt->box_qty=$data['hd_sd_box'];}
            else{$dt->box_qty=$data['hd_hd_box'];}
            $dt->lco_id=$data['id'];
            $dt->save();
        }
    }
         return response()->json(array(
                        'status' => true,
                        'message' => 'Updated Successfully'
                    ));

        }

    }
    
    public function up_lcos(Request $request)
    {
        $rules = array(
            'lco_file' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        // process the form
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } else {
            $data = $request->all();
            $file_path = $data['lco_file']->getRealPath();

            $file_contents = Excel::load($file_path, function ($reader) {
                $reader->ignoreEmpty();
            })->get();
            if (!empty($file_contents) && $file_contents->count()) {
                $file_contents = array_filter($file_contents->toArray());
                $empty = true;

                foreach ($file_contents as $v) {
                    $v = array_filter($v);

                    if (!empty($v)) {
                        $empty = false;
                        $group = \App\Group::firstOrNew(['name' => trim($v['group_name'])]);
                        $group->name = $v['group_name'];
                        $group->save();

                        $lco = \App\CableOperator::firstOrNew(['lco_num' => trim($v['lco_number'])]);

                        
                        $lco->lco_num = $v['lco_number'];
                        $lco->group_id = $group->id;
                        $lco->name = $v['lco_name'];
                        $lco->mobile = $v['registered_mob_no'];
                        $lco->address = $v['lco_address'];
                        $lco->email = $v['e_mail_id'];
                        $lco->city = $v['city'];
                        $lco->district = $v['district'];
                        $lco->pin_code = $v['pincode'];

                        $lco->save();
                    }
                }
                
                if (!$empty) {
                    return response()->json(array(
                        'status' => true,
                        'message' => 'Uploaded Successfully'
                    ));
                } else {
                    return response()->json(array(
                        'status' => false,
                        'message' => 'No LCOs found in file'
                    ));
                }
            }
        }
    }

    public function add(Request $request)
    {
        $data = new \App\CableOperator;

        $data->lco_num = $request->all()['lco_num'];
        $data->name = $request->all()['name'];
        $data->gst_num = $request->all()['gst_num'];
        $data->mobile = $request->all()['mobile'];
        $data->email = $request->all()['email'];
        $data->group_id = $request->all()['group_id'];
        $data->address = $request->all()['address'];
        $data->city = $request->all()['city'];
        $data->district = $request->all()['district'];
        $data->pin_code = $request->all()['pin_code'];

        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function update(Request $request, $id)
    {
        $data = \App\CableOperator::find($id);
        
        $data->lco_num = $request->all()['lco_num'];
        $data->name = $request->all()['name'];
        $data->gst_num = $request->all()['gst_num'];
        $data->mobile = $request->all()['mobile'];
        $data->email = $request->all()['email'];
        $data->group_id = $request->all()['group_id'];
        $data->address = $request->all()['address'];
        $data->city = $request->all()['city'];
        $data->district = $request->all()['district'];
        $data->pin_code = $request->all()['pin_code'];
        
        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->all()['id'];
        $res = \App\CableOperator::where('id', $id)->delete();
        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function getDownload($filename)
    {
        $file_arr = explode('.', $filename);
        $count_explode = count($file_arr);
        $ext = strtolower($file_arr[$count_explode-1]);

        $mimet = [
            'txt' => 'text/plain',
            'json' => 'application/json',
            'xml' => 'application/xml',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // adobe
            'pdf' => 'application/pdf',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        ];

        if (isset($mimet[$ext])) {
            $cont_type = $mimet[$ext];
        } else {
            $cont_type = 'application/octet-stream';
        }

        //PDF file is stored under project/public/download/info.pdf
        $file= storage_path(). "/download/".$filename;

        $headers = [
            'Content-Type: '.$cont_type,
        ];

        return response()->download($file, $filename, $headers);
    }
}
