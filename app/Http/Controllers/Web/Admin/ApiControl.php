<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Excel;
use PDF; 
use App\BillInfo;
 class ApiControl extends Controller
{
    public function getbills(Request $request){
          return  BillInfo::paginate(20);
    }
    
     public function pdfview(Request $request,$id){
           $get= BillInfo::where("_id",$id)->get();
     
    foreach ($get as $data) {

    $total_amt= $data["total_amount_after_tax"];
    include app_path().'/Http/Controllers/Web/Admin/numberstoword.php';

      $tx_fee_for_pgdm=$data["fee_for_pgdm"]!=""?"9%":"";
      $tx_fee_for_pgdm_hr=$data["fee_for_pgdm_hr"]!=""?"9%":"";
      $tx_fee_for_bkfs=$data["fee_for_bkfs"]!=""?"9%":"";
      $tx_fee_for_two_programmes=$data["fee_for_two_programmes"]!=""?"9%":"";
      $tx_fee_for_three_programmes=$data["fee_for_three_programmes"]!=""?"9%":"";
      $tx_sub_total=$data["sub_total"]!=""?"9%":"";

  $html='<html >
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Tapmi</title>
         <link rel="stylesheet" type="text/css" href="/style.css">
       
      <style>
         body {font-family: arial, Arial, Helvetica, sans-serif; font-size: 12px}
         body {
         height: 297mm;/*297*/
         width: 210mm;
         }
         dt { float: left; clear: left; text-align: right; font-weight: bold; margin-right: 10px } 
         dd {  padding: 0 0 0.5em 0; }
         .pad
         {
         text-align: left;
             padding-left: 7px;
         }
         .padr{
         text-align: right;
         }
         .padding{
         margin-left: 20px;
         margin-right: 20px;
         }
         th{
              padding: 5px;
          text-align:center;
         }
         td{
              padding: 5px;
          
         }

      </style>
       
   </head>

   <body> 
      <page size="A4">
       
                 <div class="padding"  style="padding-top: 20px;">
                     <img src="logo.png" alt="" class="logo" width="100" height="80" />
                  </div>
             
           
        
           <div width="100%"> 
         <table cellspacing="0" border="1"     class="padding"  >
            <tr >
               <th colspan="7">
                  <h2 align="center">Tax Invoice</h2>
               </th>
            </tr>
            <tr>
               <th class="pad" colspan="7" >
                  Invoice No : ADM/18-20/SA/'. $data["invoice_no"] .' 

                  <lable style="float: right;     padding-right: 3px;"  ><b>Date of Invoice :   '. date('d-m-Y', $data["date_of_invoice"]) .'  </b>   </lable>
                  </th>
                  
            </tr>
            <tr>
               <th class="pad" colspan="7">GSTIN :29AAATT2248R1ZR <label style="float: right;    padding-right: 3px;"><b>PAN :</b> AAATT2248 R </label></th>
            </tr>
      
            <tr>
               <th class="pad" colspan="7">Bill to Party :   '. $data["bill_to_party"] .'  </th>
              
            </tr>
            <tr>
               <th class="pad" colspan="7"><b>Candidate Name :  '. $data["name"] .' </b></th>
              
            </tr>
            <tr>
               <th class="pad" colspan="7">Address:  '. $data["address"] .'  </th>
               <!-- <td colspan="2"></td> -->
            </tr>
            <tr>
               <th  class="pad" colspan="7">Contact No :  '. $data["contact_no"] .'   </th>
              
            </tr>
            <tr>
               <th class="pad" colspan="7">Email :  '. $data["email"] .'  </th>
              
            </tr>
          
            <tr>
               <th class="pad" colspan="4">State :  '. $data["state"] .'  </th>
               <th class="pad" colspan="3">Pincode :  '. $data["pincode"] .'  </th>
               
            </tr>
            <tr>
               <th class="pad" colspan="7">GSTIN : Unregistered</th>
            </tr>
            <!-- here start -->
            <tr>
               <th rowspan="2"> Particulars</th>
               <th rowspan="2" > Amount </th>
               <th colspan="2" >CGST</th>
               <th colspan="2" >SGST</th>
               <th rowspan="2">Grand Total</th>
            </tr>
            <tr>
               <th>Rate</td>
               <th>Amount</td>
               <th>Rate</td>
               <th>Amount</td>
            </tr>
            <tr>
               <td width="40%" class="pad"><b>Application Fee For PGDM</b></td>
               <td width="7%" class="padr"> '. $data["fee_for_pgdm"] .' </td>
                       <td width="6%" class="padr"> '.$tx_fee_for_pgdm.'</td>
               <td width="6%" class="padr">'. $data["pgdm_cgst"] .'</td>
               <td width="6%" class="padr">'.$tx_fee_for_pgdm.'</td>
               <td width="6%" class="padr">'. $data["pgdm_sgst"] .' </td>
               <td class="padr" width="10%">'. $data["pgdm_grand_total"] .'</td>
            </tr>
            <tr>
               <th class="pad">Application Fee For PGDM - HR</th>
               <td class="padr">'. $data["fee_for_pgdm_hr"] .' </td>
               <td class="padr">'.$tx_fee_for_pgdm_hr.'</td>
               <td class="padr">'. $data["pgdm_hr_cgst"] .' </td>
               <td class="padr">'.$tx_fee_for_pgdm_hr.'</td>
               <td class="padr">'. $data["pgdm_hr_sgst"] .' </td>
               <td class="padr"> '. $data["pgdm_hr_grand_total"] .'</td>
            </tr>
            <tr>
               <th class="pad">Application Fee For PGDM -BKFS</th>
               
               <td class="padr">'. $data["fee_for_bkfs"] .' </td>
               <td class="padr">'.$tx_fee_for_bkfs.'</td>
               <td class="padr">'. $data["bkfs_cgst"] .' </td>
               <td class="padr">'.$tx_fee_for_bkfs.'</td>
               <td class="padr">'. $data["bkfs_sgst"] .' </td>
               <td class="padr"> '. $data["bkfs_grand_total"] .'</td>
            </tr>
            <tr>
               <th class="pad">Application Fee For TWO PROGRAMES</th>
                <td class="padr">'. $data["fee_for_two_programmes"] .' </td>
               <td class="padr">'.$tx_fee_for_two_programmes.'</td>
               <td class="padr">'. $data["two_cgst"] .' </td>
               <td class="padr">'.$tx_fee_for_two_programmes.'</td>
               <td class="padr"> '. $data["two_sgst"] .' </td>
               <td class="padr"> '. $data["two_grand_total"] .'</td>
            </tr>
            <tr>
               <th class="pad">Application Fee For ALL THREE PROGRAMES</th>
               <td class="padr">'. $data["fee_for_three_programmes"] .'</td>
               <td class="padr">'.$tx_fee_for_three_programmes.'</td>
               <td class="padr">'. $data["three_cgst"] .' </td>
               <td class="padr">'.$tx_fee_for_three_programmes.'</td>
               <td class="padr">'. $data["three_sgst"] .'</td>
               <td class="padr"> '. $data["three_grand_total"] .'</td>
            </tr>
            <tr >
               <th class="pad">TOTAL</th>
               <td class="padr">  '. $data["sub_total"] .'</td>
               <td class="padr">'.$tx_sub_total.'</td>
               <td class="padr"> '. $data["sub_cgst"] .'</td>
               <td class="padr">'.$tx_sub_total.' </td>
               <td class="padr"> '. $data["sub_sgst"] .' </td>
               <td class="padr">'. $data["sub_grand_total"] .' </td>
            </tr>
            <tr >
               <th class="pad" rowspan="5" colspan="4"><b>Total Amount in words :<b><br><br>     '.ucfirst($total_amt).' only</th>
               <th class="pad" colspan="2">Total Amount Before Tax </th>
               <td class="padr"> '. $data["total_amount_before_tax"] .' </td>
            </tr>
            <tr >
               <!-- <th class="pad" rowspan="4" colspan="4">Rupees</th> -->
               <th class="pad" colspan="2">Add: CGST </th>
               <td class="padr">'. $data["add_cgst"] .' </td>
            </tr>
            <tr>
               <th class="pad" colspan="2">Add: SGST </th>
               <td class="padr">'. $data["add_sgst"] .'</td>
            </tr>
            <tr>
               <th class="pad" colspan="2">Total Tax Amount </th>
               <td class="padr">'. $data["total_tax_amount"] .' </td>
            </tr>
            <tr>
               <th class="pad" colspan="2">Total  Amount After Tax: </th>
               <td class="padr">'. $data["total_amount_after_tax"] .' </td>
            </tr>
            <!-- <tr >
               <th rowspan="4" colspan="4"></th>
               
               
               <td colspan="4" width="40%"></td>
               
               </tr> -->
         </table>
         <table style=" float: left;   "    cellpadding="1" class="padding">
            <tr>
               <td    style=" padding-top: 20px;">Certified that the particulars given above are true and correct. </td>
            </tr>
            <tr >
               <td style=" padding-top: 40px;" ><b>For T A Pai Management Institute</b></td>
               
            </tr>
            <tr>
            <td style=" padding-top: 30px;  "  ><b>Chief Manager</b></td>
            </tr>
            <tr>
               <td style="padding-top: 40px;color: red;font-size: 15px" ><b>Disclaimer:</b> This is a computer generated Invoice and Requires No Signature</td>
            </tr>
         </table>

      
      </page>
      
   </body>
</html>';
        $pdf = PDF::loadHtml( $html);
       // $pdf->setPaper('A4', 'landscape');

          return $pdf->stream();

 }   

       echo "<h3> No records  found .. </h3>";

      

     
          
    }
    public function pdfview1(Request $request)
    {
     // die(json_encode("sdf"));
      $id="5a27e1de26b4801f2640d2e1";
        $get= BillInfo::where("_id",$id)->get();  
      
          view()->share('response',$get);
       
         if($request->has('download')){
          // Set extra option
         //   PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
          // pass view file
            $pdf = PDF::loadView('Admin.pages.billInfo');
            // download pdf
            
            return $pdf->download('pdfview.pdf');
          }
       
        return view('Admin.pages.billInfo');
   
    }
   

    public function import_export(){
    
     
     $get=DB::collection('excel_users_new')->orderby('_id','DESC')->get();

      //return view('Admin.pages.import_export')->with("response",$get);
         
    }

    public function import_upload(Request $request){  
    require  app_path().'/Http/Controllers/Web/Admin/phpmailer/PHPMailerAutoload.php';
 
    $rules = array('import_file' => 'required');
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  return back()->with('error',$value[0]);  } 
    }
 
    set_time_limit(300);
          $c=0;
    if($request->hasFile('import_file')){         
      $path = $request->file('import_file')->getRealPath();

      $data = Excel::load($path, function($reader) {})->get();
                     
     $res['data']=array();
    
    $x=0;
	 
      
       try {
      if(!empty($data) && $data->count()){
       
       foreach($data as $value) { 
		 
			  if(!empty($value['invoice_no'])){
          $bill = BillInfo::firstOrNew(['invoice_no' => (string)$value['invoice_no']]);  
                        $mailing=isset($bill->invoice_no)?false:true;  
                       

                                     $bill->invoice_no = (string)$value['invoice_no'];
                                     $bill->date_of_invoice = strtotime($value['date_of_invoice']);
                                     $bill->bill_to_party = (string)$value['bill_to_party'];
                                     $bill->name = (string)$value['name'];
                                     $bill->address = (string)$value['address'];
                                     $bill->contact_no = (string)$value['contact_no'];
                                     $bill->pincode = (string)$value['pincode'];
                                     $bill->name = (string)$value['name'];
                                     $bill->email = (string)$value['email'];
                                     $bill->state = (string)$value['state'];
                                     $bill->fee_for_pgdm = (string)$value['fee_for_pgdm'];
                                     $bill->pgdm_cgst = (string)$value['pgdm_cgst'];
                                     $bill->pgdm_sgst = (string)$value['pgdm_sgst'];
                                     $bill->pgdm_grand_total = (string)$value['pgdm_grand_total'];
                                     $bill->fee_for_pgdm_hr = (string)$value['fee_for_pgdm_hr'];
                                     $bill->pgdm_hr_cgst = (string)$value['pgdm_hr_cgst'];


                                     $bill->pgdm_hr_sgst = (string)$value['pgdm_hr_sgst'];
                                     $bill->pgdm_hr_grand_total = (string)$value['pgdm_hr_grand_total'];
                                     $bill->fee_for_bkfs = (string)$value['fee_for_bkfs'];
                                     $bill->bkfs_cgst = (string)$value['bkfs_cgst'];
                                     $bill->bkfs_sgst = (string)$value['bkfs_sgst'];
                                     $bill->bkfs_grand_total = (string)$value['bkfs_grand_total'];
                                     $bill->fee_for_two_programmes = (string)$value['fee_for_two_programmes'];
                                     $bill->two_cgst = (string)$value['two_cgst'];
                                     $bill->two_sgst = (string)$value['two_sgst'];
                                     $bill->two_grand_total = (string)$value['two_grand_total'];

                                     $bill->fee_for_three_programmes = (string)$value['fee_for_three_programmes'];
                                     $bill->three_cgst = (string)$value['three_cgst'];
                                     $bill->three_sgst = (string)$value['three_sgst'];
                                     $bill->three_grand_total = (string)$value['three_grand_total'];


                                     $bill->sub_total = (string)$value['sub_total'];
                                     $bill->sub_cgst = (string)$value['sub_cgst'];
                                     $bill->sub_sgst = (string)$value['sub_sgst'];
                                     $bill->sub_grand_total = (string)$value['sub_grand_total'];


                                     $bill->total_amount_before_tax = (string)$value['total_amount_before_tax'];
                                     $bill->add_cgst = (string)$value['add_cgst'];
                                     $bill->add_sgst = (string)$value['add_sgst'];
                                     $bill->total_tax_amount = (string)$value['total_tax_amount'];
                                     $bill->total_amount_after_tax = (string)$value['total_amount_after_tax'];
  
                                     $res=$bill->save();
                                    // die(json_encode($res));

                                    $x++;
                                    
                                     if($mailing){ 

//die(json_encode("sSs"));
                                     $to =(string)$value['email'];
                                     $sname= (string)$value['name'];
                                     $bill_link="http://invoice.tapmi.edu.in/genarate-bill/".     $bill->_id;
                                    // $to ='chipsyinfo@gmail.com';
include app_path().'/Http/Controllers/Web/mail_template/sendmail.php';
                                    
$mail = new \PHPMailer(true);
$mail->Host = "smtp.office365.com";
$mail->Port       = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth   = true;
$mail->Username = "invoice@tapmi.edu.in";   
$mail->Password = "tapmi123$";
$mail->SetFrom('invoice@tapmi.edu.in', 'TAPMI');

$mail->addAddress( $to,  $sname);
//$mail->SMTPDebug  = 2;
$mail->IsHTML(true);
$mail->Subject  = 'Bill Invoice';
$mail->MsgHTML($message_body);
$mail->Send();
 if(!$mail->send()) {
    echo "Email not sent. " , $mail->ErrorInfo , PHP_EOL;
} else {
	$c++;
    echo "Email sent! ". $to , PHP_EOL;
}
 

                                                                         
								 }
                                      //die(json_encode(    $to));
							                	}else {
													 return back()->with('error','Invalid excel formate .');
												}
                                    
                    }
        }
        } catch (Exception $e) {
            return back()->with('error','Invalid excel formate .');
        }
  /*
    if($x>100) {
		 return back()->with('error',"Please import 100 only records at time.. Limit exceeded try again");
	 }*/
      
         return back()->with('success',$c.'  Records inserted successfully.');

      }

    }

    
    

     

}
