<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class BoxControl extends Controller
{
    public function index()
    {
        $data = \App\Box::query();

        $data = $data->paginate(10);
        return view('Admin.pages.boxes', ['boxes' => $data]);
    }

    public function get(Request $request, $id)
    {
        $data = \App\Box::find($id);

        return $data;
    }

    public function getData(Request $request)
    {
        $data = \App\Box::query();
        $data = $data->paginate(10);

        return $data;
    }

    public function update(Request $request, $id)
    {
        $data = \App\Box::find($id);
        
        $data->subscription = $request->all()['subscription'];
        $data->maintenance = $request->all()['maintenance'];
        
        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }
}
