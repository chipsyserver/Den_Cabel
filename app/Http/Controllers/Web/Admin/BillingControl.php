<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Excel;

class BillingControl extends Controller
{
    public function index()
    {
        $data = \App\Billing::query();
        $data = $data->paginate(10);
        return view('Admin.pages.billings', ['billings' => $data]);
    }

    public function get(Request $request, $id)
    {
        $data = \App\Billing::find($id);
        return $data;
    }

    public function getData(Request $request)
    {
        $data = \App\Billing::query();
        $data = $data->paginate(10);
        return $data;
    }

    public function add(Request $request)
    {
        $data = new \App\Billing;

        $data->lco_num = $request->all()['lco_num'];
        $data->name = $request->all()['name'];
        $data->gst_num = $request->all()['gst_num'];
        $data->mobile = $request->all()['mobile'];
        $data->email = $request->all()['email'];
        $data->group_id = $request->all()['group_id'];
        $data->address = $request->all()['address'];
        $data->city = $request->all()['city'];
        $data->district = $request->all()['district'];
        $data->pin_code = $request->all()['pin_code'];

        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function update(Request $request, $id)
    {
        $data = \App\Billing::find($id);
        
        $data->lco_num = $request->all()['lco_num'];
        $data->name = $request->all()['name'];
        $data->gst_num = $request->all()['gst_num'];
        $data->mobile = $request->all()['mobile'];
        $data->email = $request->all()['email'];
        $data->group_id = $request->all()['group_id'];
        $data->address = $request->all()['address'];
        $data->city = $request->all()['city'];
        $data->district = $request->all()['district'];
        $data->pin_code = $request->all()['pin_code'];
        
        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->all()['id'];
        $res = \App\Billing::where('id', $id)->delete();
        if ($res) {
            return [
                'status' => true
            ];
        }
    }
}
