<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class GroupControl extends Controller
{
    public function index()
    {
        $data = \App\Group::query();
        $data = $data->paginate(10);
        return view('Admin.pages.groups', ['groups' => $data]);
    }

    public function getData(Request $request)
    {
        $data = \App\Group::query();
        $data = $data->paginate(10);
        return $data;
    }

    public function get(Request $request, $id)
    {
        return \App\Group::find($id);
    }

    public function add(Request $request)
    {
        $data = new \App\Group;
        $data->name = $request->all()['name'];
        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function update(Request $request, $id)
    {
        $data = \App\Group::find($id);
        $data->name = $request->all()['name'];
        $res = $data->save();

        if ($res) {
            return [
                'status' => true
            ];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->all()['id'];
        $res = \App\Group::where('id', $id)->delete();
        if ($res) {
            return [
                'status' => true
            ];
        }
    }
}
