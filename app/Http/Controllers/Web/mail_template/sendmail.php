﻿<?php
$message_body = '<table border="0" cellspacing="0" cellpadding="0" style="max-width:600px">
    <tbody>
        
        <tr height="16"></tr>
        <tr>
            <td>
                <table bgcolor="#4184F3" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #e0e0e0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px">
                    <tbody>
                        <tr>
                            <td height="20px" colspan="3"></td>
                        </tr>
                        <tr>
                            <td width="32px"></td>
                            <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#ffffff;line-height:1.25"><img src="https://www.tapmi.edu.in/mail_logs/tapmi-brand.png" width="50px"> <span style="    top: 36px;
    left: 104px;
    position: absolute;">TAPMI Manipal </span> </td>
                            <td width="32px"></td>
                        </tr>
                        <tr>
                            <td height="18px" colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table bgcolor="#FAFAFA" width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px">
                    <tbody>
                        <tr height="16px">
                            <td width="32px" rowspan="3"></td>
                            <td></td>
                            <td width="32px" rowspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                                <p>Dear '.$sname.',</p>
                                <p> This is your invoice copy of bill generated ... </p>
                                <div style="text-align:center">
                                    <p dir="ltr"><strong style="border:1px solid #3079ed;text-align:center;color:#fff;text-decoration:none;font-size:13px;font-weight:bold;min-height:16px;padding:10px 14px;line-height:16px;border-radius:2px;display:inline-block;min-width:54px;background-color:#4d90fe;margin-bottom:5px;cursor: pointer;"    ><a href='.$bill_link.' style="color: white;"> Click Here </a></strong>
                                    </p>
                                </div>
                                <p>Sincerely,
                                    <br>TAPMI</p>
                            </td>
                        </tr>
                        <tr height="32px"></tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr height="16"></tr>
        <tr>
            <td style="max-width:600px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#bcbcbc;line-height:1.5"></td>
        </tr>
        <tr>
            <td>
                <table style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#666666;line-height:18px;padding-bottom:10px">
                    <tbody>
                        <tr>
                            <td>This email canot receive replies.
                                <br>© TAPMI Edu.</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>';
?>
