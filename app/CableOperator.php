<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CableOperator extends Model
{
    protected $fillable = [
        'lco_num',
        'group_id',
        'name',
        'mobile',
        'address',
        'email',
        'city',
        'district',
        'pin_code'
    ];

    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }
}
