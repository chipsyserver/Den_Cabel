<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class  Ico_boxes extends Model
{
  protected $table ='lco_boxes';
   protected $fillable = [
        'box_id',
        'box_qty',
        'lco_id'
    ];
}
